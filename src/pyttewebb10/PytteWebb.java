package pyttewebb10;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * A simple web server that only supports HTTP 1.0 and HTTP 1.1.
 *
 * Only GET and HEAD Methods are supported and if the request is valid the file
 * is sent with status and headers. If the request was not valid or the file was
 * missing an error file is sent back to the client.
 *
 * @author Thomas Aggesj�
 * @author Einar Largenius
 */
public class PytteWebb
{

    private final int mPort;
    private String requestMethod;
    private String resource;
    private String protocol;
    private String status;

    private final String mDefaultFile = "index.html";
    private final String m400ErrorFile = "error400.html";
    private final String m404ErrorFile = "error404.html";

    /**
     * Constructs a simple web server.
     *
     * @param port The port number that the server can listen to
     */
    public PytteWebb(int port)
    {
        mPort = port;
    }

    /**
     * The server starts listening on a port.
     *
     * @throws IOException In case of a IO problem with the socket
     */
    public void runServer() throws Exception
    {

        ServerSocket serverSocket = new ServerSocket(mPort);
        while (true)
        {
            this.handleRequest(serverSocket.accept());
        }
    }

    /**
     * Handle a single client request
     *
     * @param clientHandlerSocket Socket associated with the request
     * @throws IOException If there was a problem reading or writing
     */
    private void handleRequest(Socket clientHandlerSocket) throws IOException
    {
        DataOutputStream writer = new DataOutputStream(clientHandlerSocket.getOutputStream());
        BufferedReader reader = new BufferedReader(new InputStreamReader(clientHandlerSocket.getInputStream()));

        String request = reader.readLine();

        String fileName = this.requestChecker(request);

        writer.writeBytes(headerHandler(fileName));

        System.out.println("request: " + request);
        System.out.println("requestmethod: " + requestMethod);
        System.out.println("resource: " + resource);
        System.out.println("protocol: " + protocol);
        System.out.println("status: " + status);
        if (requestMethod.equalsIgnoreCase("GET") || status.equalsIgnoreCase("400 Bad Request"))
        {
            byte[] file = this.readFile(fileName);
            this.sendFile(writer, file);
        }
        writer.close();
        clientHandlerSocket.close();
    }

    private String headerHandler(String fileName) throws IOException
    {
        String header = new String();
        Path file = FileSystems.getDefault().getPath(fileName);
        header += "HTTP/1.0 " + status + " \r\n";
        header += "Content-type: " + Files.probeContentType(file) + "\r\n";
        header += "Content-length: " + Integer.toString((int) file.toFile().length()) + "\r\n";
        header += "\r\n";
        System.out.println(header);
        return header;
    }

    /**
     * Parse the request and determines what file to send to the client. If the
     * request is valid and the requested file exists, the file is returned. If
     * the request is not valid or the file doesn't exist the error file is
     * returned
     *
     * @param request The request from the client
     * @param errorFile File to return if the the request was not valid or the
     * requested file did not exist
     * @return The requested file if the request was valid and the file exists.
     * Otherwise the error filer is returned
     */
    private String requestChecker(String request)
    {
        String fileName = m400ErrorFile;
        status = "400 Bad Request";
        String[] requestLine = request.split(" ");

        if (requestLine.length > 2)
        {
            requestMethod = requestLine[0];
            resource = requestLine[1];
            protocol = requestLine[2];

            if ((protocol.equalsIgnoreCase("HTTP/1.0") || protocol.equalsIgnoreCase("HTTP/1.1")) && (requestMethod.equals("GET") || requestMethod.equals("HEAD")))
            {
                if (resource.equals("/"))
                {
                    fileName = mDefaultFile;
                    status = "200 OK";
                }
                else if (Files.exists(FileSystems.getDefault().getPath(resource.replaceFirst("/", ""))))
                {
                    fileName = resource.replaceFirst("/", "");
                    status = "200 OK";
                }
                else
                {
                    fileName = m404ErrorFile;
                    status = "404 Not Found";
                }
            }
        }
        resource = fileName;
        return fileName;
    }

    /**
     * Reads a small file into a byte[]
     *
     * @param fileName Name of the file we want to read
     * @return The file as a byte[]
     * @throws IOException In case of a IO problem while reading
     */
    private byte[] readFile(String fileName) throws IOException
    {
        Path file = FileSystems.getDefault().getPath(fileName);
        return Files.readAllBytes(file);
    }

    /**
     * Send a file with the provided writer
     *
     * @param writer Writer we want to use
     * @param file The file we want to send
     * @throws IOException In case of a IO problem while sending
     */
    private void sendFile(OutputStream writer, byte[] file) throws IOException
    {
        writer.write(file);
    }
}
