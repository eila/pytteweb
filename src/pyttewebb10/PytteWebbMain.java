package pyttewebb10;

/**
 * Program entry point. Usage: java PytteWebbMain [port]
 *
 * A simple web server that only supports HTTP 0.9. Only GET is supported and if
 * the request is valid the file is sent back without status or headers. If the
 * request was not valid or the file was missing an error filer is sent back to
 * the client.
 *
 * @author Thomas Aggesj�
 * @author Einar Largenius
 */
public class PytteWebbMain
{

    /**
     * Program entry point. Usage: java PytteWebbMain [port]
     *
     * Port is optional. If no argument is given port 8080 is used as default.
     *
     * @param args optional, may contain port number
     */
    public static void main(String[] args)
    {
        int port = 8080;
        if (args.length > 0)
        {
            try
            {
                port = Integer.valueOf(args[0]);
            }
            catch (NumberFormatException nfe)
            {
                System.out.println("Non integer value for port number, using " + port + " instead");
            }
        }
        PytteWebb server = new PytteWebb(port);
        System.out.println("Starting server listening on port " + port);
        try
        {
            server.runServer();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
